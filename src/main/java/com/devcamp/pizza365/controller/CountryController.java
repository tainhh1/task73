package com.devcamp.pizza365.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.repository.CountryRepository;

@CrossOrigin
@RestController
public class CountryController {
	@Autowired
	CountryRepository countryRepository;

	@GetMapping("/countries")
	public ResponseEntity<Object> getAllCountry() {
		try {
			return new ResponseEntity<>(countryRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries/{countryId}")
	public ResponseEntity<Object> getCountryById(@PathVariable Long countryId) {
		try {
			Optional<CCountry> countryFound = countryRepository.findById(countryId);
			if (countryFound.isPresent()) {
				return new ResponseEntity<>(countryFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found countries with specified id: " + countryId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/countries/count")
	public ResponseEntity<Object> countCountries() {
		try {
			return new ResponseEntity<>("All countries are: "+countryRepository.count(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/countries/check/{countryId}")
	public ResponseEntity<Object> checkCountryById(@PathVariable Long countryId){
		try {
			if (countryRepository.existsById(countryId)) {
				return new ResponseEntity<>(countryRepository.existsById(countryId), HttpStatus.FOUND);				
			} else {
				return new ResponseEntity<>(countryRepository.existsById(countryId), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/countries/countryCode={countryCode}")
	public ResponseEntity<Object> getCountryByCountryCode(@PathVariable String countryCode) {
		try {
			if (countryRepository.findByCountryCode(countryCode) != null) {
				return new ResponseEntity<>(countryRepository.findByCountryCode(countryCode), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found countries with specified code: " 
						+ countryCode,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/countries/countryName={countryName}")
	public ResponseEntity<Object> getCountryByCountryName(@PathVariable String countryName) {
		try {
			if (countryRepository.findByCountryName(countryName) != null) {
				return new ResponseEntity<>(countryRepository.findByCountryName(countryName), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found countries with specified name: " 
						+ countryName,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("countries")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry newCountry) {
		try {
			return new ResponseEntity<>(countryRepository.save(newCountry), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("countries/{countryId}")
	public ResponseEntity<Object> updateCountry(@PathVariable Long countryId, @RequestBody CCountry newCountry) {
		try {
			Optional<CCountry> countryFound = countryRepository.findById(countryId);
			if (countryFound.isPresent()) {
				CCountry updateCountry = countryFound.get();
				updateCountry.setCountryName(newCountry.getCountryName());
				updateCountry.setCountryCode(newCountry.getCountryCode());
				return new ResponseEntity<>(countryRepository.save(updateCountry), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found countries with specified id: " + countryId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/countries")
	public ResponseEntity<Object> deleteAllCountries() {
		try {
			countryRepository.deleteAll();
			return new ResponseEntity<>("All countries were successfully deleted", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/countries/{countryId}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long countryId) {
		try {
			if (countryRepository.findById(countryId).isPresent()) {
				countryRepository.deleteById(countryId);
				return new ResponseEntity<>("Country with specified id:" + countryId + "was succefully deleted",
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found countries with specified id: " + countryId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
