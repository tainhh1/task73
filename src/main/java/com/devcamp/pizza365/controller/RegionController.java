package com.devcamp.pizza365.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;

@CrossOrigin
@RestController
public class RegionController {
	@Autowired
	CountryRepository countryRepository;
	@Autowired
	RegionRepository regionRepository;

	@GetMapping("/regions")
	public ResponseEntity<Object> getAllregions() {
		try {
			return new ResponseEntity<>(regionRepository.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries/{countryId}/regions")
	public ResponseEntity<Object> getRegionByCountryId(@PathVariable Long countryId) {
		try {
			Optional<CCountry> countryFound = countryRepository.findById(countryId);
			if (countryFound.isPresent()) {
				return new ResponseEntity<>(countryFound.get().getRegions(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found country with specified id: " + countryId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries/{countryId}/count/regions")
	public ResponseEntity<Object> countRegionByCountryId(@PathVariable Long countryId) {
		try {
			Optional<CCountry> countryFound = countryRepository.findById(countryId);
			if (countryFound.isPresent()) {
				return new ResponseEntity<>("All regions of country is: " + countryFound.get().getRegions().size(),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found country with specified id: " + countryId,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/regions/{regionId}")
	public ResponseEntity<Object> getReionById(@PathVariable Long regionId) {
		try {
			Optional<CRegion> regionFound = regionRepository.findById(regionId);
			if (regionFound.isPresent()) {
				return new ResponseEntity<>(regionFound.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found region with specified id: " + regionId, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/regions/regionCode={regionCode}")
	public ResponseEntity<Object> getRegionByRegionCode(@PathVariable String regionCode) {
		try {
			if (regionRepository.findByRegionCode(regionCode) != null) {
				return new ResponseEntity<>(regionRepository.findByRegionCode(regionCode), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found regions with specified code: " 
						+ regionCode,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/regions/regionName={regionName}")
	public ResponseEntity<Object> getRegionByRegionName(@PathVariable String regionName) {
		try {
			if (regionRepository.findByRegionName(regionName) != null) {
				return new ResponseEntity<>(regionRepository.findByRegionName(regionName), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found regions with specified name: " 
						+ regionName,
						HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/regions/check/{regionId}")
	public ResponseEntity<Object> checkRegionById(@PathVariable Long regionId) {
		try {
			if (regionRepository.existsById(regionId)) {
				return new ResponseEntity<>(regionRepository.existsById(regionId), HttpStatus.FOUND);
			} else {
				return new ResponseEntity<>(regionRepository.existsById(regionId), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("countries/{countryId}/regions")
	public ResponseEntity<Object> createRegion(@RequestBody CRegion newRegion, @PathVariable Long countryId) {
		try {
			Optional<CCountry> countryFound = countryRepository.findById(countryId);
			newRegion.setCountry(countryFound.get());
			return new ResponseEntity<>(regionRepository.save(newRegion), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/regions/{regionId}")
	public ResponseEntity<Object> updateRegion(@PathVariable Long regionId, @RequestBody CRegion newRegion) {
		try {
			Optional<CRegion> regionFound = regionRepository.findById(regionId);
			if (regionFound.isPresent()) {
				CRegion updateRegion = regionFound.get();
				updateRegion.setRegionCode(newRegion.getRegionCode());
				updateRegion.setRegionName(newRegion.getRegionName());
				return new ResponseEntity<>(regionRepository.save(updateRegion), HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found region with specified id: " + regionId, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/regions")
	public ResponseEntity<Object> deleteAllRegions() {
		try {
			regionRepository.deleteAll();
			return new ResponseEntity<>("All Regions were successfully deleted", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/regions/{regionId}")
	public ResponseEntity<Object> deleteRegionbyId(@PathVariable Long regionId) {
		try {
			if (regionRepository.findById(regionId).isPresent()) {
				regionRepository.deleteById(regionId);
				return new ResponseEntity<>("Region with id: " + regionId + " was successfully deleted", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("can't found region with specified id: " + regionId, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
